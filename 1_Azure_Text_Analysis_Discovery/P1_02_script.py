
#imports

import random
import os, sys
from azure.ai.textanalytics import TextAnalyticsClient
from azure.core.credentials import AzureKeyCredential

# VARIABLES
ENDPOINT = "https://francecentral.api.cognitive.microsoft.com"

# Functions

def authenticate_client():
    ta_credential = AzureKeyCredential(key)
    text_analytics_client = TextAnalyticsClient(
            endpoint=ENDPOINT, 
            credential=ta_credential
            )
    return text_analytics_client


def language_detection(client, paragraphs):   
    try:
        response = client.detect_language(documents = paragraphs, country_hint = '')
        return(response)

    except Exception as err:
        print("Encountered exception. {}".format(err))

def usage():
    print("usage: python [file]\n \
        Options, arguments, environnement variables:\n \
        [file]\t File containing paragraphs to translate (view README fore more information\n\
        -h \t display this message\n\n \
        Environnement variables: \n \
        AZURE_ANALYTICS\t You Azure API Key")
    
# Prompting the user

print("Hello\n")
print("Analyse du texte en cours...\n")
if (len(sys.argv) != 2 or sys.argv[1] == "-h"):
    usage()
    exit()
    
if ("AZURE_ANALYTICS" not in os.environ):
    key = input("La clé d'API n'a pas été detectée dans les variables.\n Merci de rentrer votre clé d'API\n")
else:
    key = os.environ["AZURE_ANALYTICS"]
    
# opening text files, and filtering with needed indexes
paragraphs = open(sys.argv[1], "r",  encoding="utf8").read().splitlines()


# creating azure client instance
client = authenticate_client()

# send our paragraphs for analysis 
result = language_detection(client, paragraphs)

analysed_texts = [doc for doc in result if not doc.is_error]

# Display API's results

print("Voici les résultats de l'analyse:\n\n")

for idx, doc in enumerate(analysed_texts):
    print("Le paragraphe {} \na été détecté en '{}' ({}), avec un score de confiance de {}\n\n".format(
            paragraphs[idx], doc.primary_language.name, doc.primary_language.iso6391_name, doc.primary_language.confidence_score
        ))
    if doc.is_error:
        print(doc.id, doc.error)

# %%
