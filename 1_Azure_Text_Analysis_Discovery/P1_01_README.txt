Time used to finish the project: **1 day**

This project is a simple exploration of Azure Text Analysis API.

A Python script is used to send sentences from language X and receive Azure's API prediction about what language it's written in.


Usage:

python P1_02_script.py [file]

The [file] parameter is the file containing the test sentences to send to the API. To make it easier to write and modify, the user just has to separate each sentence by a line break. The script will then deal with each sentence.

For instance:

Bonjour, je suis un chatbot
Hi, I'm a chatbot
Hola, soy un chatbot

To use the script, you'll need to provide your Azure's API key. For security reasons, this key can't be passed as a parameter nor be stored in the script. You have 2 options for this:

- Create an environment variable name 'AZURE_ANALYTICS' with your API key as the value. You may need to reboot your computer.

- Paste your API on script's prompt. (This won't appear in your shell history, and you'll need to paste it at each script run)