# Master of science Data Scientist

This repo will contain all the projects I did for my Data Scientist Master of Science.

For all modules, you'll find the topic, the notebook(s), and a detailed report (in French) explaining what I've done, how, and why. I also included French (and English translated) professional assessor's report for all modules. All assessors are professionals in Machine Learning / Data Science field (PhD, Computer vision specialist, Data Scientist in biology etc.)

All topic are based on real-life, professional situations:

[P1: Discover Azure Text Analytics API](1_Azure_Text_Analysis_Discovery)

[P2: Data Analysis - Help an NGO build a smart city](2_Help_Paris_Become_A_Smart_City)

[P3: Exploiting big data to make Public Health Agency's informations understandable to all](3_ANOVA_Public_Health_Agency-Understanding_Nutrition_Data)

[P4: Credit risk analysis - build a scoring algorithm](4_Bank_Credit_Risk_Analysis)

[P5: Marketing - Customer clustering for an e-commerce platform](5_Marketing_clustering_Ecommerce)

[P6: Automatic image labelling / Automatic sentiment analysis](6_NLP_Sentiment_Analysis_Image_Classification)

[P7: Deep Learning Sentiment Analysis Bad buzz detection](7_Bad_Buzz_Detection_Deep_Learning)

[P8: Semantic Segmentation for Autonomous vehicle develoment](8_Semantic_Segmentation_Autonomous_Vehicles)

[P9: Content recommendation mobile app development](9_Recommender_Systems)

[P10: Chatbot development for vacations booking](#)

[P11: Project scoping, management, legal risks etc.](#)
