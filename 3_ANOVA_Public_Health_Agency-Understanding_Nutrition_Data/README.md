Time used to finish the project: **2 weeks**

# Objective

Nutrition data is complex. A lot of information co-exist and it's really hard to extract useful concepts from all this, even for PHA sometimes.

In this context, a public health agency wishing to make its nutrition data accessible to everyony called out a competitive tender.

The public dataset ([Open Food](https://world.openfoodfacts.org/)) is pretty big and contains a lot of columns ([find their dscription here](columns_description.txt))

After an extensive EDA, I chose to focus on French "Nutriscore", ranged from A (best) to E (worst) and used ANOVA / hypothesis tests to verify if sugar/salt/fat amounts are significantly impacting the Nustriscore.

# Process

- Data fetching
- Data cleaning
- Data imputing
- Data analysis
- Specific EDA linked to target (French "Nutriscore")
- Multivariate analysis
- PCA
- Hypothesis tests
- Weg page with Voilà! library

# Tools, libs, algorithms used

- General stack (Numpy, Pandas, Seaborn / Matplotlib)
- IQR cleaning
- KNN Imputing
- PCA
- Dataviz tools (heatmaps, boxplots, piecharts, etc)
- Voilà! library

# Over-delivering

- Used GridSearchCV to hypertune KNN imputer
- 23 pages detailled report of the work done

![Report extract](https://lh3.googleusercontent.com/fife/AAWUweVgeGRwtiql7LgPAXx8BCygR_6MdhWPJ_0WGCt_ALmTZB0awIxT5JDDrfBHvYJ3uDYcBQzRu9fG3sfGClq5w9BPKt2ol1otzK3ySmVqflLJrqb7leIF5MoLZe__6z3WrmnKLfT955_Li9b6oY1PX_yEH053UvGSM19i-9ufCurtDJD1lTTUG730Q7_HK-MxvFfgGMH3ghUDtbP6bcrvZy0euKEf6gr9_LMRoip64zfqlyTY3kZEOA9ZOUMZP3YmmhDcRb3dhB-qL9nYp8i8MWIhIzALeGuGS5XMmXspGFVBg-WhVOt2yeJHPVfIGjar4XjojtnWlpdR1ba4rZQBoJXTMp9bmleqjXrM6K38hc48u6tCiahBCbgwKVplYQb9v1YoYjufZxy4DN0VVR5LYdlzD3vuZiSWWwf-z7w4pu1ZlHk_3pgfBEPy4IhLwN5XYbaqXJnKZWJrIcoOBWiuV6NOqPjtvKTasAucgT8fg9Aii1FazVlQwkgS3xs9AVhWH-iNA5dlW8cBc61y1MIFKfn6EOr-j2Ps1bp9sSdr4aXOLU-PmbYgs_lRCPNQhL6udrW7M0hiRiKfer50RED6KBHpyOmvrb_JXhL3nh9WRnPcWQb-vfG-AxqAW6jKLG1D0mJgN8kEOSQHMIO3-xzROw6bwayjvmUHI6dq0p9CmMxYaduCmUO1_1Tx-MmsugS_d24XYmj4HeYscIL1awQ5b0MwFSchcKY-T8o=w1920-h923-ft)
