
# # #==================================================#
# # #              Streamlit Dashboard  
#    Fetches tweets and send them to a ML model for    #
#     sentiment analysis prediction.                   #
#                                                      #  
#  input: written tweets or hastag                     #
#  output: prediction score coverted to sentiment +    #     
#  sentiment distribution                              # 
# #                                                    # 
# # #==================================================#



##=============##
#    IMPORTS    #
##=============##    
import streamlit as st
import json, requests
import urllib.request
import matplotlib as mpl
import altair as alt
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
mpl.rcParams['font.size'] = 9.0


##=============##
#    PLOTTING   #
##=============## 
def plot(res, user_input):

    df = pd.DataFrame.from_dict(json.loads(res)).rename(columns={0: "sentiment"})
    
    df['sentiment'] = df.sentiment.apply(lambda x: decode_sentiment(x))

    st.set_option('deprecation.showPyplotGlobalUse', False)
    st.write("### Résultats de la prédiction", df.sort_index())
    fig = plt.figure()
    df['sentiment'].value_counts().plot.pie(
    legend=False,
    autopct='%1.1f%%',
    figsize=(4, 4),)
    fig.suptitle('Sentiment distribution', fontsize=12)
    plt.xlabel('', fontsize=8)
    plt.ylabel('', fontsize=8)

    st.pyplot()
    pass

##=====================##
#  SENTIMENT DECODING   #
##=====================## 
def decode_sentiment(x):
    print("x -> ", x)
    if float(x) < 0.5:
        return "Négatif"
    else:
        return "Positif"
    
##=====================##
#    FETCHES TWEETS     #
#   FROM TWITTER API    #
##=====================##    
def get_tweets(hashtag):
    import requests, twitter
    
    api = twitter.Api(consumer_key="#####",
            consumer_secret="############",
            access_token_key="##########",
            access_token_secret="##########")

    results = api.GetSearch(raw_query="q=%23{}&limit=20&result_type=recent".format(hashtag))
    tweets = []
    for res in results:
        tweets.append(res.text)
    result = get_predictions(tweets, True)
    return result

##=====================##
#       SENDS TWEETS    #
#    GETS PREDICTIONS   #
##=====================## 
def get_predictions(data, hashtag = False):

    if hashtag == False:
        data = data.split('\n')
    print("input: ", data)
    body = str.encode(json.dumps(data))

    url = 'http://e5f0a1fc-a9f2-4a16-8bad-d30a7dba8015.francecentral.azurecontainer.io/score'

    headers = {'Content-Type':'application/json'}

    req = urllib.request.Request(URL, body, headers)

    try:
        response = urllib.request.urlopen(req)
        # print("RZSP: ", response)
        result_ = response.read().decode("utf-8")
        
        print("RESULT ", result_)
        
        return result_
    except urllib.error.HTTPError as error:
        print("The request failed with status code: " + str(error.code))

        print(error.info())
        print(error.read())
        return (error.info())
# # #===========================================#
# # #              Streamlit Code               #
# # #===========================================#
desc = "Uses an LSTM neural network trained on *The Lord of the Rings*. Check out the code [here](https://github.com/christian-doucette/tolkein_text)!"
header = {'Content-Type': 'application/json'}
URL = "http://e5f0a1fc-a9f2-4a16-8bad-d30a7dba8015.francecentral.azurecontainer.io/score"
st.title('Air Paradis Sentiment Analysis')


user_input = st.text_area('Enter tweets (one per line)')
if st.button('Get predictions'):
    res = get_predictions(user_input)
    plot(json.loads(res), user_input)
st.write ("\nOR\n")
st.markdown('Get predictions from hastag __(don\'t use the # symbol)__')
hashtag = st.text_input('')


if st.button('Get hashtag sentiments'):
    res = get_tweets(hashtag)
    plot(json.loads(res), "")

    