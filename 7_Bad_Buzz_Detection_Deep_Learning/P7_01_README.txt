To use the Azure API, you'll need to provide your Azure's API key. For security reasons, this key can't be passed as a parameter nor be stored in the script. You have to create an environment variable name 'AZURE_ANALYTICS' with your API key as the value. You may need to reboot your computer.

The Azure Studio Classic does not offer an interface export, I'll then provide a screenshot of what I did and do a demo during the presentation. I can't provide my personnal Web service key but I'll still provide the notebook created to test the service.

Regarding the custom model, I can't provide my personnal Web service key either. I'll also do a demo of that during the presentation, on the Azure portal + a Streamlit dashboard