Time used to finish the project: **3 weeks**

# Objective

We're hired by an airline company with bad reputation on Twitter. We have to build a model to predict the sentiment of a tweet, using 3 methods:

- Azure API
- Azure Studio Classic
- Custom model Development + endpoint

We used a [1.6M tweets dataset](https://www.kaggle.com/kazanova/sentiment140), available on Kaggle.

# Process

# Azure API

- Access and credentials creation
- Data cleaning
- Usage of Azure Python SDK
- Getting results from API

## Azure Studio

- Upload of the dataset on Blob Storage for better performance
- Use of studio to load, partition, split data
- Pre-processing
- Feature selection
- Model choice, training
- Training pipeline test
- Deployment and test with Azure, custom code and REST client

## Custom model

- Data fetching, cleaning
- Pre-processing (manual + Spacy / TextBlob)

  - Tokenisation
  - Custom Stop words
  - Lemmatization

- Vetorization (TF-IDF, Bag of words, Word2Vec, GLove)
- ML Model comparison (LR, RFC, Adaboost)
- RNN comparison (SimpleRNN, GRU, LSTM, bi-directional LSTM )
- Hypertuning
- Model registration and service deployment on Azure ACI

![Report extract NLP](https://lh3.googleusercontent.com/fife/AAWUweUurvoTSOlijB3XCa4xfZ-zyWKA1egZwIdugrbFgueVR2p0A4slxeT486zX1ayR-XR2EJQBTtI2GITY0Dr6Koi0uMYZfMrZnYc9EFxOYa6J1zb4br62g889B8CcCx6yLI_zx44dXsAPIHxZDTL7PTQI-V9m4nMg1TkSTCmgafBT97CvzRR9M2OPi7dmKNusEQ87Ou_OR5b8eSN1D0CU8oTPdV_AOICLuoEDoWGdjfzPj3kwLj9GLZrxXmdc7-UomNSG-5ojxX3jcSUQiX2QzJeCMB3XUb6XI5ViFrdjpQzg0Ig0vTC93oNS4Haz1WDMvMoyEF0FkxlGpA-XwCThNNqqxltgMrHqMpfrgD6jolmYKipcfWPXkHsFEr27RAVX8-ps-5AV0-B788FkN3Uhpo8Zm8fginhh_jVoSe14R6n77Ker5ESESznzhL3niDzjpifhm9BGCX8_RREMIz8UUGhJHVlgimrXePdG66EMo8J-GFihWOG5UkdM35xXlxLJ501oDq247QvSgcQ7EmQjeTqyMQlfY4_RWmoSbpsCKV-jOM9Jq64roCe9xZAy2YNUf6cQdpyJ4T1rK0zGPgD1xEy_uQinig7SSIeHI8ue-JuK2mQsqvKGmbzeS4hm5KaR3vAxiiz-JfRHrLD-3Q5ab_XvT66LGJ_0op2gd-ghpnmGMaTt3CSzQ5fHBE50tDe39fgB5kYR6GVOHY-fTRmpFl2Z82S2UcbxoAc=w1920-h611-ft)

# Tools, libs, algorithms used

- General stack (Numpy, Pandas, Seaborn / Matplotlib)
- NLP libs: nltk, Spacy, TextBlob
- Bag of Words, TF-IDF, Word2Vec, Glove
- Dataviz libs: WordCloud
- Azure Text Analytics API
- Azure Studio Classic
- LR, RFC, Adaboost, SimpleRNN, GRU, LSTM
- GridSearchCV, KerasClassifier
- AUC, ROCAUC Score
- Azure ACI with custom configuration
- OpenCV
- Streamlit

# Over-delivering

- Streamlit dashboard to predict a written tweet or a full batch of tweets based on a #hashtag
- Made stats and proposed an alert method based on the streamlit tool built
- 38 pages detailled report of the work done

![Streamlit dashboard](https://lh3.googleusercontent.com/fife/AAWUweVP-QVYG1PEmgitHKfqypo0gxrLIQl-9TZwRW842OwDbmayBytlfgmAxFouiwqjnRzTmTmLC93K5IAXpefa0ekGs_I3h3KDhOZTe4gFCHiVH0Yqvk6813TiR2BAkLnaUZMDtvoMAuLoukkzx9rvQK4p8IO7d4jk7c-A5kFT_XNuIkZAqaktgxVFZrgoBY-7pyYvwOJ8ZNjWSC5m_Tr1XZWbONPr5CA9rwEtgTVykdOM5J6cqGaCrtSFKboLb_fY_FNYFBsJ_O-LJvTRzt9dICIR2Qg57IIuSVk_WLUPfBkFETtxt_xDupACHp-EXxD1zFlTK4jTxDZBRwovrNelQd0VslBARqnXq-fmvtiojR98-Jm_ku2KWo7Jgiq5VNudCuB6VL4CNQgcUlbHw5xqHcBm7LyOXLZutI7nhHSAV6C5hV-NbaU2GPsP1v0G7whSs9PYYuOywEQm0AFaqSAckuDDaD894JsacXOHtg-lijEx9MGHKouuiebSNpKjKxbIqLmS2lZD0FPkiZRm712zM1__eL0gqES_MWBAJQbJTq_4PxURO9-X_6NVauYNUtUYV034Tz9j7-5uc6QwH4CLRLVT4cXgYuuGQ5nhtZggRjts8hmRY7F13R678NU2mDUPBlfARt-yEliM09xgzHPP827TtKuuwoc6tlcVszFXLBxvThG8a8qhmefxlt43nMsMspf_cEOE_pePuLn5hz7vSzHC4utstxqkoE4=w1920-h611-ft)
