Time used to finish the project: **1.5 week**

# Objective

This project is about smart cities. I act as a junior Data Analyst.
To be successful il the challenge _Data Is For Good_ , I neet to exlore and analyse open datas to help the city be more "green".

The dataset is available [here.](https://opendata.paris.fr/explore/dataset/les-arbres/map/?dataChart=eyJxdWVyaWVzIjpbeyJjb25maWciOnsiZGF0YXNldCI6Imxlcy1hcmJyZXMiLCJvcHRpb25zIjp7fX0sImNoYXJ0cyI6W3siYWxpZ25Nb250aCI6dHJ1ZSwidHlwZSI6ImNvbHVtbiIsImZ1bmMiOiJBVkciLCJ5QXhpcyI6ImlkYmFzZSIsInNjaWVudGlmaWNEaXNwbGF5Ijp0cnVlLCJjb2xvciI6IiMwMDMzNjYifV0sInhBeGlzIjoidHlwZWVtcGxhY2VtZW50IiwibWF4cG9pbnRzIjo1MCwic29ydCI6IiJ9XSwidGltZXNjYWxlIjoiIiwiZGlzcGxheUxlZ2VuZCI6dHJ1ZSwiYWxpZ25Nb250aCI6dHJ1ZX0%3D&location=13,48.86844,2.30945&basemap=jawg.streets)

An extensive EDA was done to identify the target, relationships, repartitions, locations, distances and help Paris' gardeners optimize their workday.

I chose to focus on trees labeled 'noteworthy', since they're situated in heaviy touristic areas and are representative of the town landscaping.

# Tools used

- Virtualenv
- Plotly interactive maps
- Seaborn / Matplotlib
- Dataviz tools (heatmaps, boxplots, piecharts, etc)

# Over-delivering

- 20 pages detailled report of the work done

![Report extract](https://lh3.googleusercontent.com/fife/AAWUweUAInOycJxEbS-UFqF--_MJM6bYXILVy81BJEPTr6tSEUVSOLZMU9cXnOtsRQZ_3JOYZgyppyPI9yzPUxK-Q_QTCaCFprHiYOxOppYu8pv1fg8FP2RnNJWXzO9v6q5vmeC7j-eG8LTE1fR0oaJjGF5U9FR7JEGrJK30zUqRdo9JC5GMu2PgQCouFq55KscSnPOikKRT-VLKfGZoX84rOqZT05DFUh0XuVfyyChF16ywHdMrtP0PMsmuuQrVTCRnSdjTPADWFzOgrqdaXkqqZiSurSvKEpEnSbV3dOWk40MZceGYyLCr8qy0Y7SPL8ue_Nk6bBFgv8HKdXab6TmcEikh13PtLqDQueZdbvfyRb-YciWVfDvZcRELzCyjmRe3-MFWWvYLa6RxAMKvDF3CtKIn60pKfv7UwUFEqAGM1_lWnKa5_yy-h321G22hoFsB0XeY-L96F-7PZDEt0z1gEIXuU9tjqeAGNvudQTgQY5RfqoRSdyWLcRQLp7q43oSnmhPL4HYGQPTOsuOgrgJ0AwmQxAsA9CmWfk118i9RdkibpeWMXaohuQOWJAHNu0YwDrtFZbFUi6AOh_R55WEWR-xKLN33joGPo2LCxM7VBqd_DTXtPkt65PoLW1GlzipumcXkz8BIqW3U3NwGu9JTr5TSg2Nj-eELAOPkxy3h7-PBzAVxo3jA1iPzO8ba2lsVXBa6g0r6NnyMNnZtkYWQnXrVTzoEgETFcNY=w1920-h581-ft)
