Time used to finish the project: **3 weeks**

# Foreword

Due to git 50Mo/file limitation, the notebook has 2 parts

# Objective

We're hired by Olist, an online Brazilian ecommerce markeplace to help them cluster and understand their customer base.

For that, we can use datas like the number or orders made, timestamps of order, mode of payment, category of order, city, seller etc. Data is anonymous, we only have customer id.
The datasets are the ones from [Kaggle Brazilian Ecommerce thread](https://www.kaggle.com/olistbr/brazilian-ecommerce).

The deliverable is giving Olist team a clustering they can use for their marketing team. We have to use unsupervided learning.

# Process

- Data fetching, Exploratory Data Analysis, Data cleaning
- Datasets aggregation and processing
- Classes balancing with SMOTE
- PCA + T-SNE
- First comparison of models
- Feature engineering to improve performance
- Model comparisons with improved data
- Hypertuning to improve model with GridSearchCV
- Final clustering
- Clusters stability
- Dataset visualisation by cluster
- Marketing strategies proposal

# Tools, libs, algorithms used

- General stack (Numpy, Pandas, Seaborn / Matplotlib)
- RFM score
- PCA and T-SNE
- K-Means, DBSCAN, Hierachical Clustering
- Silhouette and Davies-Bouldin scores
- Features importance
- Feature engineering (data aggregation, variable creation, polynomial features, etc)
- GridSearchCV
- Plotly interactive maps

# Over-delivering

- Proposed several marketing stategies to adopt based on the clustering and analysis done
- Proposed ways to improve the clustering by advising type of additional data to collect
- 44 pages detailled report of the work done

![Report extract](https://lh3.googleusercontent.com/fife/AAWUweXSKVRfZd_aFqmn5ujjAi_3rz2vsGH7ChHe1a3E3_JWr6gdpczd8zDMaa0qr0g7Xzbz6-Qds0S9fyB6_jmC_ZFWDwe5MjxqgpCqDhk4tFyKiimD1oiiLEND6qK-lv5aotLhwEy_sp2td_VMqoD7j-iuBIJopAN-r_6dIOaL3F-Yp-7Ff0LYUrmxFvve0SkaIsB95DvJdBjNEVHfB_Doex8VdGiIfNxg4TG1y6is_1Lr5_JSvhOBgo4BGVPyCmV-k2e0BfOD1kqr2dIbR54qLg5ijIxG3EPeVRscwDB-p95aiudA6-wpcbPdisZgnsDkO7Jac3yxZCAd1RV7cwwxcq5jhH6qqdFtHWndfUJUVTTJgxO39llb5fVCm2ZBpp63-1hO9zNE7npRgmUoNVY7q5rJyMZ2Px6PPCn1vsko84mq7_XnX4YSLQFWFab8COk_BC-Byz3LU82Cic8jkNi44yPyWBa1PVgUHBpiQwyjJObyGvtLIZp8VUW1B2RjWvGSlZi_X99aMWu5LLIvmPgHPE6AqYiZ2bnJscmMrPWw75zmbvxEYalLHXVMmdTDxRz2x2MQ-BJe-D8TO6iymPYqynYsBsN5Ylz_IZgYAMfFQuILtb8vpN4ajnhO73WtyuavFVIrHels3FnDfhk8qsc96yesuMtiJV3ofEIqR1fBQ0tyu3Z86GOdHeMENkZ9xYffw7zgnPpNs4l-lQ1tbqvyunTHF-VyaMK19vc=w1920-h581-ft)
