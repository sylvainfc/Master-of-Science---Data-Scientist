Time used to finish the project: **3 weeks**

# Objective

We're hired as a Data Scientist in a financial company. The account managers have to decide if they can grant a loan or not.

For that, they have loads of informations regarding the credit applicant, divided between many datasets: loan history, credit card balances, socio-demographic contexts etc...

To make the process easier, we are asked to compare models for a future scoring algorithm. The model will have to be interpretable as the account managers must be able to explain to the applicant what drove their decision.

The datasets are the ones from [Kaggle Home credit default risk](https://www.kaggle.com/c/home-credit-default-risk/data).

# Process

- Data fetching
- Exploratory Data Analysis
- Data cleaning
- Treating imbalanced classes
- Data processing
- Model comparison (SGD, LR, RFC, LGBM)
- Feature engineering to improve performance
- Hypertuning to improve model
- Creation of Sklearn Pipeline to automate the process
- SHAP for model explainability

# Tools, libs, algorithms used

- General stack (Numpy, Pandas, Seaborn / Matplotlib)
- Metric choice (precision, recall, f1 score)
- SGD, LR, RFC, LGBM
- Sklearn, Imblearn
- Features importance
- Feature engineering (data aggregation, variable creation, polynomial features, etc)
- GridSearchCV, RandomizedSearchCV
- ColumnTransformer, FeatureUnion, pipelines
- SHAP

# Over-delivering

- Built a final model and achieved a 0.72191 score on this Kaggle competition on 1st and 2nd submissions (late submission)
- Created an automated script to compare, train, hypertune models with different metric
- Created a script to let account manager to input datas and automatically get result + explanations from the model
- 25 pages detailled report of the work done

![Report extract](https://lh3.googleusercontent.com/fife/AAWUweXZqAtfNJbMcBh2Ku-kf4m9tEgZpAWGor56Ev74HxxbxJLCzNoDcbza5NYssUYRYWUBSscg3OX9ooOSZOXTROYidFWOaAxye27_9-3FmGGRLgzaHoLXFRiHag3RNopXy-cGxnOp9j8O_Qv-_eyws6Rrbkb1oZxeIyLLfxDs0u7huOuz1Wf1PNlj2BvwRnf0Kb3u2kKEyBQx2FCmkrwa78lcNtaM8s7K2_3ExmyGRQ--LgKVd7_GcDH0Ga6V_oJsM4wNRgP-dPrSogWmfEtZr4rXvmyciBpN7eltwJ4fGSSYCjagWsKSjeComNaSv3FXHY6ZAyBf0diLevDllGo99vmQ5K5uT9hTOHjHVboDVeey6IDZb3o9EIW9D5D2SXKQLdT-P3KOZoFmcXJECjIbDFqAC-3E59rNRUWiT6vM2tf4y0Rf3c89k1v3DOLnentYW79z_IizQeotz-l_YsFBLC-1hLXEe7QgaNsAHgCbH3sLAPAVAb582vnfbkuhtO_0fJvpI0Wp4L_JnyjWwLBaMBvcZqZNBJ_aFpzB0xcfkO8tKMoZARzMh86LbincrSIb8-XUMEZ2Y7kIjf_NPO0I33k0BeXoZvY7HKa8InPSPJjebnYBfvjn3lnKw2L8la2TTivqFnV3xBgIiyv3KGZeFRf8o1s5N1vrlu7HAOTGfBqk5k-ZJJDz1Zh6UcUFausRqkXPcFiJKmg39pqyx3FMxDJJHKmPY0ra9bM=w1920-h581-ft)
