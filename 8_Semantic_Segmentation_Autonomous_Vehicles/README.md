Time used to finish the project: **6 weeks**

# Objective

We're a 3 Data Scientist R&D team, working on autnomous vehicles. The first member acquire real life images and pass them to me. I have to pre-process them and create a model to do an efficient semantic segmentation so that the 3rd member can fetch result on my API and feed the decision system.

We used the [cityscapes dataset](https://www.kaggle.com/kazanova/sentiment140), available on Kaggle.

# Process

## Data Management

- Creation of a Data Generation custom class to deal with dataset size (12Go)
- Implementation of data augmentation with Albumentations library
- Storage of data on Azure DataLake storage gen2 (made for low-latency DL needs)

## Model training

- Comparison with non-augmented / augmented datas
- Comparison of non-trained MobileNet / Unet
- Comparison of Imagenet Unet / FPNet / Linknet
- Tests with class weights / ethical weigts / no weights
- Custom callbacks creation
- Training on full, augmented dataset

## Deployment

- Development of a Flask API
- Model registration and service deployment on Azure ACI
- Deployment of App via Github CI

![Report extract NLP](https://lh3.googleusercontent.com/fife/AAWUweW9dqncWlmxRzAtaGRY2H5c8MBAOW6rJJ4_r5ab9YmjhjQZZ_H4M5zxoy9azVtEJn05HYB0tUlrLzUp7tWDvfq4xGqyqyRUjnOrqjK_pAMj2bLYidRoA6dUsxmiHUC8-lxLldEBett4ExGFDI8p5ubs7Ev-JjW3VfKYSAWQKHvdu91CVjHlT7r1MBDyUpUJfjZvcf4Zc3Ye-k9vpULECIPAdEtl8MMqfrNBIp8vUw3O4JWBTt_ZALT8Ybo5rexCBrp8lTD7ZS7LK_yUkkv5O8UQl45fpnOe9YYB20X4WIRQw8plzoYJF4ouVrgxr0R_qZnYyDnPAiUw6kPwsOED-DotgdXg8w2lHqZmeOh2i4O_fIoJgxv_--8wjklEgwQbpjvrRp1nkjVBEJJty7MH08ODFyzQEpwlt073H5chZyW94Z44nNHdqmtHsygyFrxqx5aKAj_FK8iXKiNkSDCJG428o_MbXie889atRgeFTA3qg2QkaSOnrTpR0cmVSA3DaiqO2Fkg5kEF7_PSWbz1YdgF_1J0adZGZzdCOETtGH3SVKGcI2hGmNRWL1PMoRfrvug8NgvWeqlBXXoTkedcZqgw67TWRtRLIcJVuNn2q4ja-TMTngaXqTg5_Mnwg6hZ1znZ1twEKntzlxqSsBHxFWJ0XTAOiK4r0PQa1745zRQnTfs_W3Hx_VrS7NNJSePh9M79ue5nymeoTgj5CDAWeDconC1Y94CGgW0=w1920-h611-ft)

# Tools, libs, algorithms used

- General stack (Numpy, Pandas, Seaborn / Matplotlib)
- Albumentations
- Keras Sequential custom class
- OpenCV
- EarlyStopping, reduceLROnPlateau, ModelCheckpoint callbacks
- MobileNet, Unet, Linknet, FPNet
- Segmentation_models
- Focal / IoU / Dice losses
- Dice / IoU metrics
- Flask
- Azure DataLake Storage gen2

# Over-delivering

- 34 pages detailled report of the work done

![Streamlit dashboard](https://lh3.googleusercontent.com/fife/AAWUweVZhvKNCda7uIz9WdtnbDwsXUIPnTEe_TCSekG1MVON97n4UHu1MsnaaNLX9oibBzf5DFW1H1tPiAdaJOwx8weupswuv7FSEKOniI7H-1QWLkFU28JVFvyjjj5vMSE1tl1Let4Or7S1Ky9tL9wt3l8IDTM4IgACr8rKFDApm9UFX_uFujrF3pHzHoJ92gMB5MLU-ZYhIFxpqXDuku1LwYCchAAxw5zDsSIOK9wPyDG9PqgJsLzcq2lTzGZXAlgLjXwiJx4AIcIwmwkffTNrNa1faKQXps8ReK6sCzUujB6aaawtMfeCNCDHqzJiijOcJMSN3NyLDFFaLrJ7V4NrlTv1oOvo1bpaNo7xNgiKjYIWmTcfLiZxUqrx-x9CZ0ccfBYxnjrYGVuav8sl88K856EjmcLNkYLLJJJ0VWeHHETheTsIs5wmHDKYH4QXVkxKe2PeDyDBaiTTvV6g-YU9xslNObFuHWAMXTGsZmx52DPWhe3hEQBDF0usCTRD_nJ6sCQJZZKouwYGZp3SKyZfZ3Ovs5JT3ena1TzAaIrUNcKJ6qhvyT_zGXBlFelVMA7xww_8-pB-TeN99ZkBvNPHH0-dASxMXIsAIwo5waHkPrYcfjoD-FXDJYU07yV8VFwlSiYzNm7wKHB4sH-8qWQT7PoiP3sYE5EFC9fJh_D7BIL7vLNzVAQfWPUwzgcnUbnjk3bz9tMcHcBuZaHy_StYx6Ea6IyK9GZfSXU=w1920-h611-ft)

![Streamlit dashboard](https://lh3.googleusercontent.com/fife/AAWUweUO411Wo3Ew7UTQRAVyJnTefpp0503M-YkW4UaaLL7Qzjh_vcz26Wxbhl5BJMbJ7dZN0oDe0j90QLEQHRXCwrc1XeeJXBHT18uNvLRToOgn3OTPUHdd4WTw3g-np6cffCidm1Gls5xYsLtTKUwUhVdh_Z85Iy3488nN2nu1NUjQN6j0higITUKDOKqdFr9vI9zaY4X0KIhGGWOMxcm762SrhN1tT9DcvcfPiN-cU9w9XNuYk8G1eQvETRE0WEib05kjIO4O0EEpkeQImGCUKjSoUw2ZHhsEbbRlEZkFVWjo3icwcB7U2fUF68EfU1ITbbxCD1vfdsf3e0gymRX_noY7TZ9oosAWbG6hafFWEtX4eLONl8JR8AeohgQNJ5X6-JHv110dc3OA4SeGrvT9KuDpIvo--pkNjJan1ooLclO2Rw86VZsXAQkR77z95iBRZG6a22xEheTal5mUDcPS6rb9pvAbhLRcxVJGkZBq-azqV6W8QoguA9GyxOdh3ojQqSmBYOFuLY5BZvpoigBmaHo1dqxD0hE348z1p2PANAO9vsT9MZGbfcy8uMSmTP5WwFg8B6GwbNl1fUHI49wHJ0ihLM3GYN_OXv-P4wofxd0DDxtmXv-Ys2AGIdVo_OKII4PtvU3qnYlJPGeR5h4fWtDFCqc0fqzqp84QpNLja0IRco6fRGFwyTOHgyv7lvXgZ6GcNdskabniXpD70t2R80OjcN7MCeQMHtg=w1920-h611-ft)
