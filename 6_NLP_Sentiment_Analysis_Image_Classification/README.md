Time used to finish the project: **4.5 weeks**

# Objective

We're hired a Data Scientist in a new startup, building a product putting customers and restaurants in touch.

2 main goals for the product:

- Detect dissatisfaction topics based on reviews
- Automatic classificatiion on images uploaded by users

We can use the [Yelp Dataset](https://www.yelp.com/dataset), but I chose to use [Yelp API](https://www.yelp.com/developers/graphql/guides/intro) with GraphQL. Query are easily buildable, and that ensures us to always have up-to-date datas without having to store a whole dataset.

# Process

- Data fetching
- Exploratory Data Analysis
- Data cleaning

## NLP

- Semantic analysis
- Pre-processing (manual + Spacy / TextBlob)

  - Tokenisation
  - Stop words
  - Lemmatization

- Frequency analysis
- Vectorization with Bag of Words, TF-IDF, Word2Vec
- Dimension Reduction (PCA / T-SNE)
- Sentiment analysis with TextBlob
- Clustering
- Topic extraction with Wordcloud, Word2Vec, LDA, Gensim

  ![Report extract NLP](https://lh3.googleusercontent.com/fife/AAWUweWXHBaX33Qgs-hzoCCuUM2F1pdATrmaQtvY6HYMkq2vHP4Tl_8A6Blc2caZkYBEvbZsJk5-DOJPDhg3gVKDqy2mnPeXWlxTuH37paUPHVwG4R2N19eOjbJHBngnYfKFqI0cJm_dFQlb_vgNhbVJTSh_RHGrojYLrD_7mNAmB7YueH8llXL9QK6ZsYjEsN-dEkQLHKcaUEezRMGU-G6TwGUiCuGOYj2tVmid5LBKVcxMUlIeefORrGPAx2IMX2s3tPJNHJIpbsVOV36pG2-AgW-y7UhgK2UxgdxMpBXTM0tT-nHow0FRxWgTGHAr6DN-04pNREiaczLL7UiYzV7YB9uV0pog6neYjsW5SDScB0zEmr0hjnIcnIi43gEmruxF6T9xQMtPGXeEeQw9y8EEuLJXSlzizr1mW_D3X5vnHIKeT2CUVWP6qAMU9A7sPPuDiYQGBL_frt87oyBTZRMM2aDHLV8amSOkCXJMFdRKfVcPtn1TIeiok7aLZ1BGf1c80mnmaKvK7j1SOM6Vko_wbm-CAgfOYHMETyAjoCkrN6DH2ZGqV_ZkLyi60HbjfwLAn6Y7IB33cadaFeyiRTTdn54uMbgMIgZZi8E71CifSr17rvLqerLBzF5avdswcxJIr4w4LbZ_DORMItnMeRxb91c4SMXCSbODAYNCBg9ACml-q70usA6Xb6jODHUkQHpEUt88CbkG-IVwCeWFGED10rJE3YnftOfkhQo=w1920-h581-ft)

## Image classification

- Pre-processing (Greyscale, histogram balancing...)
- Bag of features, Key features extraction with ORB
- Dimension Reduction
- Clustering
- Transfer Learning VGG166

# Tools, libs, algorithms used

- General stack (Numpy, Pandas, Seaborn / Matplotlib)
- NLP libs: nltk, Spacy, TextBlob
- Dataviz libs: WordCloud, Gensim
- PCA and T-SNE
- K-Means clustering
- Silhouette and Davies-Bouldin scores
- Image feature extraction: ORB, SIFT, SURF
- Transfer Learning: ImagenNetVGG16
- HTML export with Voilà!

# Over-delivering

- Used a CNN model in addition to asked K-Means for the last clustering
- Obtained a 0.96875 validation_accuracy
- 37 pages detailled report of the work done

![Report extract VGG](https://lh3.googleusercontent.com/fife/AAWUweWX4oCy9wgtaaCfzW9fpssL26qGa89-cjIxqENB1YG8BItRnZ5XIUUkHCVolr2XdGbVV8uatKTv8ns7ROYs0-IgJfcFVcMdZzW3WC5CcE-O-g_8SO0_xd-LzAgirxanWtldcPCL-kdHceNs0tV6AcVpVQKeWUNLVO0hixKk8ozE_V7L8hfaUL6Ut1HT-b3w0-6m1PrIQBzp3apliRMMHInIPjhFPxF6PB8jBdrCDcdWhdGbt8dB_1NoW8Y2-Mw9rIbWEoIX-LJqTQyGWTVAMyEqOKw0nyfWnBSNRqM3Wk9K1PtuFqkhyCWQdhEogLB90Q2BJBbpgkJOqUCV1eKK2XeJ1_elmvmxuluVbtsvYulu96r01-t6DKMdIMKBzl13YnAYTkhzPVIrnji-5y9JiVoDYdHy02MdZFNjGEHytdq_HaDJnUi31cKrKpByujUSuWDaZI-l_vSpAi54BrXPEskF-9T7WzZXAw84gHoDW6EeLvXBNKpz1CwuZ-Z34-z6a4SpiddkFt0EVKzPoUHG34muJqxMmivDGshybmNfi0QOLbbM7QOFjGd5gByx-_sSOjESEF3vEo_92sQpcx0j0Y5lu14mEkmMjNYk60kt6QmlqNSnd4Df5SeaDEYN09bk7m3cim9v492v6WthiQjZb6Y2lchSpU2aJqJYkMhR9HiH3LzHQv44tncmPg2tyj3am8oTqSieZF8brIzbm7OGJ7L-Il2497sgA-g=w1920-h581-ft)
