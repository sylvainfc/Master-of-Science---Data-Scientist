Time used to finish the project: **2.5 weeks**

# Foreword

The topic is really wide and interesting. As the project presentation is only 20min long, I did a 2h30 long live stream on Twitch around this topic. You can view the replay (in French) here : https://www.youtube.com/watch?v=h8ZIHR8CTwc

# Objective

We're a brandnew startup building a mobile app that will recommend books and articles to the customers. Based on the [Globo.com dataset](https://www.kaggle.com/gspmoreira/news-portal-user-interactions-by-globocom), we have to build some ratings as we don't have any explicit ratings, test, and deploy the chosen recommender system on Microsoft Azure.

Some MLOps was also involved, as we needed to thing about the future architecture.

# Process

## Data Management

- Data aggregation
- Exploratory Data Analysis
- Feature Engineering

## Model training and comparison

- Creation of a relevent, implicit, weighted rating for each interaction
- Creation of an Evaluation class for metrics computation (cosine_similarity, Recall@K, MAP@K, MAR@K)
- Creation of a class for each tested model
- Popularity model (to address cold start problem)
- Content-based model on embeddings
  - cosine_similarity
  - sigmoid kernel
- Collaborative Filtering
  - Nearest Neighbors user based
  - Matrix Factorization item based
- Hybrid model with and without weights
- Surprise Library SVD
- Implicit Library
  - ALS
  - Matrix Factorization
- LightFM
  - Learning-to-rank with WARP

## Deployment

- Azure Functions with HTTP triggers, CosmosDB and Blob Storage bindings
- Models registration and service deployment on Azure ACI
- Test of Databricks and Spark for a scalable, cost-effetive deployment

# Tools, libs, algorithms used

- General stack (Numpy, Pandas, Seaborn / Matplotlib)
- Recommender Systems basics
- KNN / ALS / SVD models
- Surprise Library
- Implicit Library
- LightFM Library
- Precision@K, recall@k, MAP@K / MAR@K, cosine, MRR..
- Azure Funtions
- Azure Cosmos DB / Blob Storage
- Databricks / Spark
- React Native

# Over-delivering

- 50 pages detailled report of the work done
- Improvement of the React Native app to reflect a bit more the goal of the app
- 2h30 long live-stream on Twitch, on Recommender Systems + live coding.
